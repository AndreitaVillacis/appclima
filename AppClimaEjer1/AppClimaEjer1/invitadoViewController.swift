//
//  invitadoViewController.swift
//  AppClimaEjer1
//
//  Created by andrea villacis on 25/10/17.
//  Copyright © 2017 andrea villacis. All rights reserved.
//

import UIKit
import CoreLocation

class invitadoViewController: UIViewController, CLLocationManagerDelegate{

    
    //MARK:- Outlets
    
    @IBOutlet weak var weatherGuest: UILabel!
    let locationManager = CLLocationManager()
    
    @IBOutlet weak var NameCityLabel: UILabel!
    //MARK:- atributes
   // var bandera = false
    
    //MARK:- viewDidLoad
    override func viewDidLoad() {
        
        super.viewDidLoad()
        locationManager.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled(){ //se ve si el usuario da permiso
            locationManager.delegate = self //estamso conformando el delagdo cuando acabe responde a esta clase
            locationManager.startUpdatingLocation()//comienza a capoturar la unbicacion del usuario
        }
        
       
        
    }
    
    //MARK:- LocationManager Delgate
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location = manager.location?.coordinate
       // if !bandera {
            ConsultarPorUbicacion(lat: (location?.latitude)!,
                                  lon: (location?.longitude)!
            )
        
            //bandera = true
        //}
        locationManager.stopUpdatingLocation()
        
    }

    
    //MARK:- Actions
    
    private func ConsultarPorUbicacion (lat : Double, lon: Double ) {
        
        let service=Servicio()
        service.ConsultarPorUbicacion(lat: lat, lon: lon) { (weather, name) in
            DispatchQueue.main.async {
                self.NameCityLabel.text = name
                self.weatherGuest.text = weather
            }
        }
    }
    
    
}
