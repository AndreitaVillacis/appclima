//
//  CiudadViewController.swift
//  AppClimaEjer1
//
//  Created by andrea villacis on 31/10/17.
//  Copyright © 2017 andrea villacis. All rights reserved.
//

import UIKit

class CiudadViewController: UIViewController {
//apikey fcc77a091a707630e9ef6b84e7961697
    
    
    //MARK:- Outlets
    
    @IBOutlet weak var cityTextField: UITextField!
    @IBOutlet weak var weatherLabel: UILabel!
    
    //MARK:- ViewController lifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }

    
    //MARK:- Actions
    @IBAction func consultarButtonPressed(_ sender: Any) {
        let service = Servicio()
        service.consultarProCiudad(city: cityTextField.text!) { (weather) in
            
            //el error morado es porque estamso cogiendo algo de la interfaz que no esta en el mismo hilo
        //y se arregla con el dispachequenue
            
            DispatchQueue.main.async {
                self.weatherLabel.text = weather
                
            }
            
        }
        
    }
    
    
   

}
