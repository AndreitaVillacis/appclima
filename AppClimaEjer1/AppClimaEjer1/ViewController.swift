//
//  ViewController.swift
//  AppClimaEjer1
//
//  Created by andrea villacis on 25/10/17.
//  Copyright © 2017 andrea villacis. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    
    
    //MARK:- Outlets
    
    
    
    @IBOutlet weak var usuario: UITextField!
    @IBOutlet weak var contraseña: UITextField!
    
    //MARK:- ViewController Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    //es para esconder el teclado
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
      usuario.text=""
        contraseña.text=""
        usuario.becomeFirstResponder()//para quitar el foco es con el resign find responds
        
    }
    
    
    //MARK:- Actions
    
    @IBAction func entrarButtonPressed(_ sender: Any) {
        let user = usuario.text ?? ""
        let password = contraseña.text ?? ""
        
        switch (user,password) {
        case ("andrea","andrea"):
            performSegue(withIdentifier: "ciudadSegue", sender: self)
            
        case ("andrea", _):
            mostrarAlerta(mensaje: "contraseña incorrecta")
        default:
            mostrarAlerta(mensaje: "usuario y contraseña incorrecta")
        }
    }
    
    private func mostrarAlerta(mensaje:String){
        let alertView = UIAlertController(title: "ERROR", message: mensaje , preferredStyle: .alert)
        let aceptar = UIAlertAction(title: "ACEPTAR", style: . default){
            action in
            self.usuario.text = ""
            self.contraseña.text = ""
            
        }
        
        present (alertView, animated: true,completion:nil)
    }
    
    @IBAction func invitadoButtonPressed(_ sender: Any) {
    }
    
}

